import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import { orderBy, findIndex } from "lodash";
import toastr from "toastr";
Vue.prototype.$http = axios;

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    products: [],
    cart: []
  },
  mutations: {
    SET_PRODUCT(state, products) {
      state.products = products.data.items;
    },
    ORDER_BY(state, prop) {
      state.products = orderBy(state.products, prop, "desc");
    },
    ADD_TO_CART(state, product) {
      let productIndex = findIndex(
        state.cart,
        p => p.product.id === product.id
      );
      if (productIndex === -1) {
        state.cart.push({ product, count: 1 });
      } else {
        state.cart[productIndex].count += 1;
      }
    },
    REMOVE_FROM_CART(state, product) {
      let productIndex = findIndex(
        state.cart,
        p => p.product.id === product.id
      );
      if (productIndex !== -1) {
        if (state.cart[productIndex].count <= 1) {
          state.cart.splice(productIndex, 1);
        } else {
          state.cart[productIndex].count -= 1;
        }
      }
    },
    SHOW_NOTIF(state, status) {
      if (status === "success") {
        toastr.success("محصول با موفقیت در سبد خرید اضافه شد");
      } else {
        toastr.error("خطا در برقراری ارتباط، لطفا دوباره تلاش کنید.");
      }
    }
  },
  actions: {
    loadProducts(state) {
      axios
        .get(
          "http://www.mocky.io/v2/5cd2bb7631000086283399be?mocky-delay=3000ms"
        )
        .then(res => res.data)
        .then(products => {
          state.commit("SET_PRODUCT", products);
          console.log(products);
        })
        .catch(err => {
          console.error(err);
          toastr.error("خطا در برقراری ارتباط. لطفا مجددا تلاش نمایید");
        });
    },
    sortProducts(state, prop) {
      state.commit("ORDER_BY", prop);
    },
    addToCart(state, product) {
      state.commit("ADD_TO_CART", product);
    },
    removeFromCart(state, product) {
      state.commit("REMOVE_FROM_CART", product);
    },
    toast(state, status) {
      state.commit("SHOW_NOTIF", status);
    }
  },
  getters: {
    products(state) {
      return state.products;
    },
    cart(state) {
      return state.cart;
    }

    // orderBy(prop) {
    //   if (prop) {
    //     state.sortedProducts = orderBy(store.state.products, prop);
    //   } else {
    //     store.state.sortedProducts = store.state.products;
    //   }
    // }
  }
});

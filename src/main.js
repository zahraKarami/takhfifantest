import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import { store } from "./store";
import toastr from "toastr";

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  toastr,
  render: h => h(App)
}).$mount("#app");
